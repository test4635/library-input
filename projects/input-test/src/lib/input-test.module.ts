import { NgModule } from '@angular/core';
import { InputTestComponent } from './input-test.component';



@NgModule({
  declarations: [InputTestComponent],
  imports: [
  ],
  exports: [InputTestComponent]
})
export class InputTestModule { }
