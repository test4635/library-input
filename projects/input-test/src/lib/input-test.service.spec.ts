import { TestBed } from '@angular/core/testing';

import { InputTestService } from './input-test.service';

describe('InputTestService', () => {
  let service: InputTestService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(InputTestService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
