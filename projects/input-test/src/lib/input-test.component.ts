import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'lib-input-test',
  template: `
    <p>
      input-test works!
    </p>
  `,
  styles: [
  ]
})
export class InputTestComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
